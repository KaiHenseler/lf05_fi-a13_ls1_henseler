
public class ArrayMitZahlen {

	public static void main(String[] args) {
		int [] zahlen = new int [10];
		for(int i=0; i< zahlen.length; i++) {
			zahlen[i] = i;
			System.out.print(zahlen[i]+"|");
		}
		System.out.println();
		
		int [] ungeradeZahlen = new int [10];
		int counter = 0;
		for(int i = 0; i < 20; i++) {
			if(i%2 != 0){
				ungeradeZahlen[counter] = i;
				System.out.print(ungeradeZahlen[counter]+"|");
				counter = counter + 1;
			}
		}
		System.out.println();
	}

}
