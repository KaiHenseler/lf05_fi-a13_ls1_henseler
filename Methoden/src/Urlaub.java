import java.util.Scanner;

public class Urlaub {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		boolean urlaub = true;
		final double EN = 0.89;
		final double CH = 1.08;
		final double SW = 10.10;
		while(urlaub == true)
		{
			System.out.println("Land:");
			String land = tastatur.next();
			System.out.println("Betrag:");
			double betrag = tastatur.nextDouble();
			if(land == "USA"||land =="usa") 
			{
				umrechnungDollar(betrag);
			}
			else if(land == "Japan"|| land =="japan")
			{
				umrechnungYen(betrag);
			}
			else if(land == "England"|| land =="england"|| land == "UK"||land == "uk") 
			{
				umrechnungPfund(betrag);
			}
			else if(land=="Schweiz"||land=="schweiz") 
			{
				umrechnungFranken(betrag);
			}
			else if(land =="Schweden"|| land == "schweden") 
			{
				umrechnungKronen(betrag);
			}
			else
			{
				System.out.println("Bitte geben Sie ein g�ltiges Land ein.\n"
						+ "Zur Verf�gung stehen USA, Japan, England, Schweiz & Schweden");
			}
			System.out.println("Reisen Sie noch?");
			String weiter = tastatur.next();
			if(weiter == "Nein"||weiter== "nein"|| weiter == "ne") 
			{
				urlaub = false;
			}
		}

	}
	public static void umrechnungDollar(double betrag) 
	{
		final double US = 1.22;
		double erg = betrag * US;
		System.out.printf("Das entspricht $%f (US-Dollar)", erg);
		//kontoabzug(betrag);
	}
	public static void umrechnungYen(double betrag) 
	{
		final double JP = 126.5;
		double erg = betrag * JP;
		System.out.printf("Das entspricht %f Yen", erg);
		//kontoabzug(betrag);
	}
}
