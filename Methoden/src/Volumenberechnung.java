
public class Volumenberechnung {
	public static void main(String[] args) {
		double a = 1;
		double b = 2;
		double c = 3;
		double h = 4;
		double r = 6;
		
		System.out.println("Wuerfel: " + wuerfel(a));
		System.out.println("Quader: "+quader(a,b,c));
		System.out.println("Pyramide: " +pyramide(a,h));
		System.out.println("Kugel: " +kugel(r));
	}
	public static double wuerfel(double x) {
		double erg = 3*x;
		return erg ;
	}
	public static double quader(double x,double y,double z) {
		double erg = x*y*z;
		return erg;
	}
	public static double pyramide(double x,double y) {
		double erg = x*x*y/3.0;
		return erg ;
	}
	public static double kugel(double x) {
		double erg = 4/3*x*x*x*Math.PI;
		return erg ;
	}
}
