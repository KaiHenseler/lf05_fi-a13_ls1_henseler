﻿// Kai Henseler FI-A-13
import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
    public static void main(String[] args){
        do {
    	double zuZahlen=fahrkartenbestellungErfassen();
        double rueckgabebetrag=fahrkartenBezahlen(zuZahlen);
        fahrkartenAusgabe();
        rueckgeldAusgabe(rueckgabebetrag);
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
        "vor Fahrtantritt entwerten zu lassen!\n"+
        "Wir wuenschen Ihnen eine gute Fahrt.");
        }while(neustart() == true);
        System.exit(0);
     }

    public static double fahrkartenbestellungErfassen(){
    	/*
    	 Durch die Verwendung von Arrays können Fahrkarten und deren Preise besser miteinander Verbunden und verwendet werden, da sie
    	 jeweils an der gleichen Stelle ihres Arrays stehen. So ist es auch einfacher weitere Tickets und Preise hinzuzufügen bzw. diese
    	 auszutauschen.
    	 In der Alten Umsetzung wurde der Preis in einem Switch Case übergeben und konnte nicht weiter genutzt werden und stand nicht direkt in Verbindung
    	 mit dem gekauften Ticket
    	*/
    	String [] fahrkarten  = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
    			"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte ABC", 
    			"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC" };
    	double [] fahrkartenpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	System.out.println("Waehlen Sie ihre Wunschfahrkarte fuer Berlin aus:");
    	for(int counter = 0; counter < fahrkarten.length; counter++) {
    		System.out.println((counter+1)+ " " + fahrkarten[counter] + " " + fahrkartenpreise[counter]+ " Euro");
    	}
    	System.out.printf("Ihre Wahl: ");
    	byte wunschticket = tastatur.nextByte();
    	System.out.print("Anzahl: ");
    	byte ticketanzahl = tastatur.nextByte();
    	if(wunschticket > fahrkarten.length || wunschticket < 1) {
    		System.out.println("Das war eine falsche Auswahl");
    		return fahrkartenbestellungErfassen();
    	}
    	else {
    		System.out.println("Ihre Wahl: "+ ticketanzahl +"x " + fahrkarten[wunschticket-1]);
    		return (fahrkartenpreise [wunschticket-1] * ticketanzahl);
    	}
    }
    public static double fahrkartenBezahlen(double zuZahlenderBetrag){
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   double eingeworfeneMuenze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rueckgabebetrag ;
    }
    public static void fahrkartenAusgabe(){
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
			Thread.sleep(250);
		}   catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgabe(double rueckgabebetrag){
        if(rueckgabebetrag > 0.0)
        {
            System.out.println("Der Rueckgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
            System.out.println("wird in folgenden Muenzen ausgezahlt:");
 
            while(rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
            {
               System.out.println("2 EURO");
               rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
            {
               System.out.println("1 EURO");
               rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
            {
               System.out.println("50 CENT");
               rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
            {
               System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
            {
               System.out.println("10 CENT");
               rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
            {
               System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
 
    }
    public static boolean neustart() {
    	System.out.println("Möchten Sie den Fahrkartenautomaten neustarten?:(j/n)");
    	char eingabe = tastatur.next().charAt(0);
    	if(eingabe == 'j' || eingabe == 'J') {
    		boolean antwort = true ;
    		return antwort;
    	}
    	else {
    		boolean antwort = false;
    		return antwort;
    	}
    }
}


