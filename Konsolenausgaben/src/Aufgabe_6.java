
public class Aufgabe_6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Überschriften
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("-----------------------\n");
		
		// Zeile 1
		System.out.printf("%-12d", -20);
		System.out.print("|");
		System.out.printf("%10.2f\n", -28.8889);
		
		// Zeile 2 
		System.out.printf("%-12d", -10);
		System.out.print("|");
		System.out.printf("%10.2f\n", -23.3333);
		
		//Zeile 3
		System.out.printf("+%-11d", 0);
		System.out.print("|");
		System.out.printf("%10.2f\n", -17.7778);
		
		//Zeile 4
		System.out.printf("+%-11d", 20);
		System.out.print("|");
		System.out.printf("%10.2f\n", -6.6667);
		
		//Zeile 5
		System.out.printf("+%-11d", 30);
		System.out.print("|");
		System.out.printf("%10.2f\n", -1.1111);
	}

}
