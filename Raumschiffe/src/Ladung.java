
public class Ladung {
	//Attribute
	private String bezeichnung;
	private int menge;
	
	//Konstruktor
	public Ladung() {
		
	}
	public Ladung(String bezeichnung, int menge) {
		super();
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	//Verwaltungsmethoden
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	@Override
	public String toString() {
		String returnString = "Ladung, " + this.bezeichnung + ", " + this.menge;
		return returnString;
	}
}
