public class RaumschiffTest {

	public static void main(String[] args) {
		// Initialisierung Ladung
		Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung schrott = new Ladung("Borg Schrott", 5);
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		Ladung forschungssonde = new Ladung ("Forschungssonde", 35);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		Ladung batleth = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
		
		// Initialisierung Raumschiffe
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(batleth);
		
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		romulaner.addLadung(schrott);
		romulaner.addLadung(roteMaterie);
		romulaner.addLadung(plasmaWaffe);
		
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		vulkanier.addLadung(forschungssonde);
		vulkanier.addLadung(photonentorpedo);
		
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reperaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraemen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
		System.out.println(klingonen.eintraegeLogbuchZurueckgeben());
		System.out.println(romulaner.eintraegeLogbuchZurueckgeben());
		System.out.println(vulkanier.eintraegeLogbuchZurueckgeben());
	}

}
