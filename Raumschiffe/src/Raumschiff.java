import java.util.ArrayList;
import java.util.Random;
public class Raumschiff {

	// Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstuktoren
	public Raumschiff() {
		
	}
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	// Verwaltungsmethoden
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	//Methoden
	/***
	 * Voraussetzung: Uebergebenes Objekt der Klasse Ladung existiert
	 * Effekt: Fuegt dem Ladungsverzeichnis des Raumschiffs die uebergebene Ladung hinzu
	 * @param neueLadung Objekt der Klasse Ladung
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	/***
	 * Effekt: Gibt die Attribute des Raumschiffes auf der Konsole aus
	 */
	public void zustandRaumschiff() {
		System.out.printf("\n%s:\n", this.schiffsname);
		System.out.printf("Torpedos: %s\n", this.photonentorpedoAnzahl);
		System.out.printf("Energie: %d Prozent\n", this.energieversorgungInProzent);
		System.out.printf("Schilde: %d Prozent\n", this.schildeInProzent);
		System.out.printf("Huelle: %d Prozent\n", this.huelleInProzent);
		System.out.printf("Lebenserhaltung: %d Pozent\n", this.lebenserhaltungssystemeInProzent);
		System.out.printf("Androiden: %d\n", this.androidenAnzahl);
	}
	/***
	 * Effekt: Gibt das Ladungsverzeichnis auf der Konsole aus
	 */
	public void ladungsverzeichnisAusgeben() {
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			System.out.printf("%s: %d Stueck \n", this.ladungsverzeichnis.get(i).getBezeichnung(), this.ladungsverzeichnis.get(i).getMenge());
		}
	}
	/***
	 * Voraussetzung: Uebergebenes Objekt der Klasse Raumschiff existiert
	 * Effekt: Schiesst einen Torpedo auf das Uebergebene Raumschiff. 
	 * (Konsolenausgabe, Verringerung der Torpedos & Treffer Methode beim Ziel wird aufgerufen
	 * @param ziel Objekt der Klasse Raumschiff
	 */
	public void photonentorpedoSchiessen(Raumschiff ziel) {
		if(this.photonentorpedoAnzahl > 0) {
			this.photonentorpedoAnzahl -= 1;
			System.out.printf("\nPhotonentorpedo auf %s abgeschossen\n", ziel.getSchiffsname());
			ziel.treffer(ziel);
		}
		else {
			System.out.println("-=*Click*=-");
		}
	}
	/***
	 * Voraussetzung: Uebergebenes Objekt der Klasse Raumschiff existiert
	 * Effekt: Schiesst mit Phasenkanone auf das Uebergebene Raumschiff. 
	 * (Konsolenausgabe, Verringerung der Energie & Treffer Methode beim Ziel wird aufgerufen
	 * @param ziel Objekt der Klasse Raumschiff
	 */
	public void phaserkanoneSchiessen(Raumschiff ziel) {
		if (this.energieversorgungInProzent < 50) {
			System.out.println("-=*Click*=-");
		}
		else {
			this.energieversorgungInProzent -= 50;
			System.out.printf("\nPhasenkanone auf %s abgeschossen\n", ziel.getSchiffsname());
			ziel.treffer(ziel);
		}
	}
	/***
	 * Voraussetzung: Objekt der Klasse Raumschiff existiert
	 * Effekt: Schadensberechnung beim getroffenen Raumschiff & Konsolenausgabe
	 * @param ziel Objekt der Klasse Raumschiff
	 */
	private void treffer(Raumschiff ziel) {
		ziel.schildeInProzent -= 50;
		System.out.printf("%s wurde getroffen!\n", ziel.getSchiffsname());
		if(ziel.schildeInProzent<=0) {
			ziel.huelleInProzent -= 50;
			ziel.energieversorgungInProzent -= 50;
			if(ziel.huelleInProzent<=0) {
				ziel.lebenserhaltungssystemeInProzent=0;
				ziel.nachrichtAnAlle("Notruf: Lebenserhaltung vernichtet!");
			}
		}
	}
	/***
	 * Effekt: Konsolenausgabe der Nachricht & hinzufügen zum Broadcast Kommunikator
	 * @param message Nachricht als typ String
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		this.broadcastKommunikator.add(message);
	}
	/***
	 * @return Inhalt des Broadcast Kommunikators
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return(this.broadcastKommunikator);
	}
	/***
	 * Effekt: Ladungen mit der Menge 0 werden aus dem Ladungsverzeichnis geloescht
	 */
	public void ladungsverzeichnisAufraemen() {
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			if (this.ladungsverzeichnis.get(i).getMenge() == 0) {
				this.ladungsverzeichnis.remove(i);
			}
		}
	}
	/***
	 * Effekt: Reperaturwert wird berechnet und den ubergebenen Schiffskomponenten hinzugefuegt
	 * @param schutzschilde Schutzschilde sollen Repariert werden Typ: Boolean
	 * @param energieversorgung Energieversorgung soll Repariert werden Typ: Boolean
	 * @param schiffshuelle Schiffshuelle soll Repariert werden Typ: Boolean
	 * @param anzahlDroiden Anzahl der zu benutzenden Androiden Typ: Int
	 */
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		if(anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}
		int schiffskomponenten = 0;
		if (schutzschilde) {
			schiffskomponenten++;
		}
		if (energieversorgung) {
			schiffskomponenten++;
		}
		if (schiffshuelle) {
			schiffskomponenten++;
		}
		Random zufall = new Random();
		int zufallsZahl = zufall.nextInt(101);
		int erfolg = zufallsZahl * anzahlDroiden / schiffskomponenten;
		if (schutzschilde) {
			this.schildeInProzent += erfolg;
			if (this.schildeInProzent > 100) {
				this.schildeInProzent = 100;
			}
		}
		if (energieversorgung) {
			this.energieversorgungInProzent += erfolg++;
			if (this.energieversorgungInProzent > 100) {
				this.energieversorgungInProzent = 100;
			}
		}
		if (schiffshuelle) {
			this.huelleInProzent += erfolg;
			if (this.huelleInProzent > 100) {
				this.huelleInProzent = 100;
			}
		}
	}
	/***
	 * Voraussetzung: positive Anzahl an Torpedos wird uebergeben
	 * Effekt: Laedt die gewuenschte Anzahl Torpedos aus dem Ladungsverzeichnis in die Abschussvorrichtungen
	 * @param anzahlTorpedos Anzahl der zu ladenen Torpedos Typ: int
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		boolean keineMehrDa = false;
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			Ladung ladung = this.ladungsverzeichnis.get(i);
			String ladungName = ladung.getBezeichnung();
			int ladungMenge = ladung.getMenge();
			if (ladungName == "Photonentorpedo" && ladungMenge == 0 ) {
				System.out.println("Keine Photonentorpedos gefunden!");
				this.nachrichtAnAlle("-=*Click*=-");
				keineMehrDa = true;
				break;
			}
			if (keineMehrDa == false && ladungName == "Photonentorpedo" && anzahlTorpedos > ladungMenge ) {
				anzahlTorpedos = ladungMenge;
			}
			if (keineMehrDa == false && ladungName == "Photonentorpedo") {
				ladung.setMenge(ladungMenge-=anzahlTorpedos);
				this.photonentorpedoAnzahl += anzahlTorpedos;
				System.out.printf("%d Photonentorpedo(s) eingesetzt\n", anzahlTorpedos);
			}
			
		}
	}	
}
