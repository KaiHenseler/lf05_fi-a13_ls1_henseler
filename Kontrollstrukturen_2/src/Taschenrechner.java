import java.util.Scanner;
public class Taschenrechner {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		int z1, z2, erg;
		char rechenzeichen;
		
		System.out.println("Bitte geben Sie die erste Zahl ein:");
		z1=tastatur.nextInt();
		System.out.println("Bitte geben Sie die zweite Zahl ein:");
		z2=tastatur.nextInt();
		System.out.println("Bitte geben Sie den gew�nschten Operatoren ein(+,-,*,/):");
		rechenzeichen = tastatur.next().charAt(0);
		
		if(rechenzeichen == '+') {
			erg = z1+z2;
			System.out.println(erg);
		}
		else if(rechenzeichen == '-') {
			erg = z1-z2;
			System.out.println(erg);
		}
		else if(rechenzeichen =='*') {
			erg = z1 * z2;
			System.out.println(erg);
		}
		else if(rechenzeichen == '/') {
			if(z2 != 0) {
				erg = z1/z2;
				System.out.println(erg);
			}
			else {
				System.out.println("Sie k�nnen nicht durch Null teilen, versuchen Sie es erneut");
				main(args);
			}
		}
		else {
			System.out.println("Die Eingabe war Fehlerhaft, versuchen Sie es erneut!");
			main(args);
		}
	}

}
