
public class Konto {

	// Attribute
	private String iban;
	private String kontonummer;
	private double kontostand;
	
	// Konstruktoren
	public Konto() {
	
	}
	public Konto(String iban, String kontonummer) {
		this.iban = iban;
		this.kontonummer = kontonummer;
	}
	
	// Verwaltungsmethoden 
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getIban(){
		return (this.iban);
	}
	public void setKontonummer(String kontonummer){
		this.kontonummer = kontonummer;
	}
	public String getKontonummer(){
		return(this.kontonummer);
	}
	public double getKontostand() {
		return(this.kontostand);
	}
	
	// allgemeine Methoden
	public void abheben(double menge){
		this.kontostand -=menge;
	}
	public void einzahlen(double menge) {
		this.kontostand += menge;
	}
	public void ueberweisen( Konto ziel, double menge) {
		this.kontostand -= menge;
		ziel.kontostand += menge;
	}
}
