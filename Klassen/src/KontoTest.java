// Test von Konto Klasse
public class KontoTest {

	public static void main(String[] args) {
		Kontobesitzer b1 = new Kontobesitzer("Max", "Mustermann");
		System.out.printf("Besitzer 1: %s %s\n", b1.getVorname(), b1.getNachname());
		
		Kontobesitzer b2 = new Kontobesitzer("Erika", "Musterfrau");
		System.out.printf("Besitzer 2: %s %s\n", b2.getVorname(), b2.getNachname());
		System.out.println();
		
		Konto psd = new Konto("1234", "12");
		System.out.printf("PSD Konto: Iban:%s Kontonummer:%s\n", psd.getIban(), psd.getKontonummer());
		
		Konto tomorrow = new Konto("5678", "56");
		System.out.printf("Tomorrow Konto: Iban:%s Kontonummer:%s\n", tomorrow.getIban(), tomorrow.getKontonummer());
		
		Konto n26 = new Konto("4321", "43");
		System.out.printf("N26 Konto: Iban:%s Kontonummer:%s\n", n26.getIban(), n26.getKontonummer());
		
		Konto bnp = new Konto("8765", "87");
		System.out.printf("BNP Konto: Iban:%s Kontonummer:%s\n", bnp.getIban(), bnp.getKontonummer());
		System.out.println();
		
		psd.einzahlen(1000);
		System.out.println("PSD 1000� Einzahlen");
		System.out.printf("PSD Kontostand: %.2f�\n", psd.getKontostand());
		psd.abheben(50);
		System.out.println("PSD 50� Abheben");
		System.out.printf("PSD Kontostand: %.2f�\n", psd.getKontostand());
		psd.ueberweisen(tomorrow, 250);
		System.out.println("PSD 250� an Tomorrow �berweisen");
		System.out.printf("PSD Konto: %.2f�\nTomorrow Konto: %.2f�\n", psd.getKontostand(), tomorrow.getKontostand());
		System.out.println();
		
		b1.setKonto1(psd);
		b1.setKonto2(tomorrow);
		System.out.printf("Alle Konten von %s %s:\n", b1.getVorname(), b1.getNachname());
		b1.gesamtUebersicht();
		System.out.println();
		
		b2.setKonto1(n26);
		b2.setKonto2(bnp);
		System.out.printf("Alle Konten von %s %s:\n", b2.getVorname(), b2.getNachname());
		b2.gesamtUebersicht();
		System.out.println();
		
		tomorrow.ueberweisen(n26, 100);
		System.out.println("Max (Tomorrow) ueberweist Erika (N26) 100�");
		System.out.printf("Tomorrow: %.2f�\nN26: %.2f�\n", tomorrow.getKontostand(), n26.getKontostand());
		System.out.println();
		
		System.out.printf("Gesamtveroegen von %s %s:\n", b1.getVorname(), b1.getNachname());
		b1.gesamtGeld();
		System.out.println();
		System.out.printf("Gesamtveroegen von %s %s:\n", b2.getVorname(), b2.getNachname());
		b2.gesamtGeld();
		
	}

}
