
public class Kontobesitzer {
	// Attribute
	private String vorname;
	private String nachname;
	private Konto konto1;
	private Konto konto2;
	
	//Konstuktor
	public Kontobesitzer() {
		
	}
	public Kontobesitzer(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}
	public Kontobesitzer(String vorname, String nachname, Konto konto1, Konto konto2) {
	this.vorname = vorname;
	this.nachname = nachname;
	this.konto1 = konto1;
	this.konto2 = konto2;
	}
	
	//Verwaltungsmethoden
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getVorname() {
		return(this.vorname);
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getNachname() {
		return(this.nachname);
	}
	public void setKonto1(Konto konto1) {
		this.konto1 = konto1;
	}
	public Konto getKonto1() {
		return(this.konto1);
	}
	public void setKonto2(Konto konto2) {
		this.konto2 = konto2;
	}
	public Konto getKonto2() {
		return(this.konto2);
	}
	
	// allgemeine Methoden
	public void gesamtUebersicht() {
		System.out.println("Konto 1: "+getKonto1().getIban()+"\nKonto 2: "+getKonto2().getIban());
	}
	public void gesamtGeld() {
		double erg = getKonto1().getKontostand()+getKonto2().getKontostand();
		System.out.printf("%.2f�\n", erg);
	}
}
